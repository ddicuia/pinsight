# Pinsight


## Question
Can a device help a person to share information about a physical space within a community?


## Deployments

### Mill Road, Cambridge 
Community sharing historical knowledge about Cambridge. There is already  a website containging already a significant amount of information contributed mostly by very commited users.

### Olympic park
Enable volunteer workers in the park community to push dialogues (interactive narratives) for park visitors to physical devices.


## Sequence communication

![](diagram.svg)




## Users

Primary: Volunteer workers of the park.
Secondary: Visitors to the park.

## RQ's

* Can physical devices help volunteer workers tell a story about the park to visitors?
* Will volunteer workers use multiple devices and link them through a common story?

## Requirements

* Users should be able to see other people's content but only edit their own.
* Users create a list of dialogues (interactive stories) that can individually be pushed to any device.

## Definitions

"app" is the smartphone app.
"client" is the physical device app.



